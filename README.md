# Seminario ML

En este seminario/semillero se discute sobre los fundamentos de algunos algoritmos de machine learning.

# Sesiones

- Regresión Lineal (a cargo de Vladimir Vargas) [[grabación](https://www.youtube.com/watch?v=2HCScXB9mnk)] [[notebook](https://gitlab.com/analist-ai/seminario-ml/-/blob/master/code/01-LinearRegression/LinearRegression.ipynb)]
- Regresión Logística y Modelos Lineales Generalizados (a cargo de Carolina Contreras) [[grabación](https://www.youtube.com/watch?v=M7gA07ZyuW8)] [[notebook]](https://gitlab.com/analist-ai/seminario-ml/-/blob/master/code/02-LogisticRegression/MLG.ipynb)
- Algoritmos de Aprendizaje Generativos (a cargo de Juan Sebástian Flórez) [[grabación](https://youtu.be/qB3qquUy0T0)][[notebook](https://gitlab.com/analist-ai/seminario-ml/-/blob/master/code/03-GenerativeLearningAlgorithms/Naive_Bayes.ipynb)][[notebook2](https://gitlab.com/analist-ai/seminario-ml/-/blob/master/code/03-GenerativeLearningAlgorithms/Gaussian_Discriminant_Analysis.ipynb)]
- Métodos de Kernel (a cargo de Nicolás Parra) [[grabación](https://youtu.be/oBMMBlU7TKA)][[notebook](https://gitlab.com/analist-ai/seminario-ml/-/blob/master/code/04-KernelMethods/04-Kernel_methods.ipynb)]
- Procesos Gaussianos (a cargo de Carlos Viviescas) [[grabación (pt. 1)](https://youtu.be/0leZLyXsUbI)] [[grabación (pt. 2)](https://youtu.be/B1xM3zV0Utk)]
- Métodos de Kernel (a cargo de Leonel Ardila) [[grabación](https://youtu.be/XDwSnGVoTBA)]
- Redes Neuronales (a cargo de Nicolás Parra y Juan Flórez) [[grabación](https://youtu.be/nSpp0YZEMOY)]
- Selección de modelos y regularización (a cargo de Carlos Viviescas) [[grabación](https://youtu.be/R2yg32Wehco)]
- Variational Autoencoders (a cargo de Juan Sebastián Flórez) [[grabación](https://youtu.be/NWsu9GuILs8)]
- Análisis de componentes principales (a cargo de Carolina Contreras) [[grabación](https://youtu.be/QdzE6TIw5-k)] [[notebook](https://gitlab.com/hubrain/seminario-ml/-/blob/master/code/PCA/PCA.ipynb)]
- Algoritmo Expectation Maximization (a cargo de Jonathan Díaz) [[grabación](https://youtu.be/4TWxkQt44n4)]
